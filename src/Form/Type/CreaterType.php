<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreaterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('firstName', TextType::class, [
                'required' => true,
                ])
            ->add('lastName', TextType::class, [
                'required' => true,
            ])
            ->add('age', TextType::class, [
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'attr' => array(
                    'class' => 'btn btn-outline-primary'
                ),
                'label' => 'SUBMIT',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);

    }


}