<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Form\Type\CreaterType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{

    public function indexAction(): Response
    {
        return $this->render('me_team.html.twig', [

        ]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function homeAction(Request $request ): Response
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();

        $form = $this->createForm(CreaterType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();

                    $form = $form->getData();
                    $user = new User();

                    $user->setLastName($form->getLastName())
                        ->setFirstName($form->getFirstName())
                        ->setAge($form->getAge())
                    ;
                } catch (\Exception $e){
                    dump($e);
                }
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('index');
            }
        }

        return $this->render('addUser.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/list", name="list")
     */
    public function getUserLists(Request $request)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('list.html.twig', [
            'list' => $user
        ]);
    }

    /**
     * @Route("/add", name="add")
     */
    public function addProdToUser(Request $request): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $product = new Product;

        $list = [];
        foreach ($users as $user){
            $list[$user->getId()] = $user->getFirstName().$user->getId();
        }

        $form = $this->createFormBuilder($product)
            ->add('user', ChoiceType::class, [
                'required' => true,
                'choices' => array_flip($list),
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'attr' => array(
                    'class' => 'btn btn-outline-primary'
                ),
                'label' => 'SUBMIT',
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $form = $form->getData();
                    if (is_null($form->getUser())) {
                        return $this->redirectToRoute('index');
                    }
                    $user =  $this->getDoctrine()->getRepository(User::class)->find($form->getUser());
                    $product = new Product();
                    $product->setUser($user);
                    $product->setName($form->getName());

                } catch (\Exception $e){
                    dump($e);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();

                return $this->redirectToRoute('index');
            }
        }
        return $this->render('addProdToUser.html.twig', [
            'form' => $form->createView(),
            'select' => $users,
            'product' => $this->getDoctrine()->getRepository(Product::class)->findAll()
        ]);
    }

}